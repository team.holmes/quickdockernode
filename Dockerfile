FROM node:12-alpine
ARG buildversion
ENV VERSION=$buildversion
RUN echo $vers
WORKDIR usr/src/app
COPY package.* .
RUN npm install --production
COPY src/ src

CMD ["npm", "start"]
