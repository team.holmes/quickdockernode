version=$1
docker build --no-cache --build-arg buildversion=$1 -t test:$1 --force-rm .

echo "====================================="
echo "Generated docker image called test:$1"