const express = require('express');
const path = require('path');

const app = new express();
app.set('view engine', 'ejs');
console.log(path.join(__dirname, '/views'));
app.set('views', path.join(__dirname, '/views'));

// app.use(express.static('public'))

app.get('/', function(req, res) {
    console.log('The user just requested a page!');
    res.render('pages/index', {
        version: process.env.VERSION || 'not set!'
    });
});


const PORT = process.env.PORT || 4000;

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
})
